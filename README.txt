
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installing
 * Configuration


INTRODUCTION
------------

The default behaviour of the Commerce Product Reference field is to inject only
actual fields from the product entity into the referencing entity. This module
adds support for injecting product entity Flag fields.


REQUIREMENTS
------------

 * Commerce Product Reference (part of Commerce)
 * Flag


INSTALLING
----------

 * Install and enable the dependencies (commerce_product_reference, flag)
 * Enable this module


CONFIGURATION
-------------

 * Enable "Display link as field" for the flag on a product entity
   (admin/structure/flags/manage/FLAG_NAME)
 * Clear caches
 * Position new extra field "Product: Flag: [FLAG TITLE]" on referencing
   entity's field display settings
