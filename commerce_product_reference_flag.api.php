<?php

/**
 * @file
 * Hooks provided by the Commerce Product Reference Flag module.
 */

/**
 * Alter product flag links.
 *
 * @param $link
 *  An array of link properties as returned by hook_flag_link().
 * @param $flag
 *  The flag_entity object.
 * @param $context
 *  An array containing the following:
 *    - action: The action the link is about to carry out, either "flag" or "unflag".
 *    - entity_id: The entity ID to check for flagging.
 *    - display_context: An array of contextual data for the referencing entity.
 *      - display_path: entity display path
 *      - entity
 *        - entity_type
 *        - entity_id
 *        - product_reference_field_name
 */
function hook_commerce_product_reference_flag_link_alter(&$link, $flag, $context) {

}
